package com.example.ms22project2.dto;
import lombok.Data;
@Data
public class StudentDTO {
    private final Long id;
    private final String name;
    private final String surname;
    private final Long age;
}