package com.example.ms22project2.contoller;
import com.example.ms22project2.dto.StudentDTO;
import com.example.ms22project2.model.StudentModel;
import com.example.ms22project2.service.StudentInterface;
import org.springframework.web.bind.annotation.*;
import java.util.Set;
@RestController
@RequestMapping(path = "/api")
public class Studentctr {
    private final StudentInterface stdinter;
    public Studentctr(StudentInterface stdinter) {
        this.stdinter = stdinter;
    }
    @PostMapping(path = "/add")
    public StudentModel addStudent(@RequestBody StudentDTO studentDTO ){
        return stdinter.add(studentDTO);
    }
    @GetMapping(path = "/getAll")
    public Set<StudentModel> getAll(){
        return stdinter.getAll();
    }
    @PutMapping(path = "/update/{id}")
    public StudentModel update(@PathVariable Long id, @RequestBody StudentDTO studentDTO){
        return stdinter.update(id,studentDTO);
    }
    @DeleteMapping(path = "/delete/{id}")
    public void delete(@PathVariable Long id){
        stdinter.delete(id);
    }
}