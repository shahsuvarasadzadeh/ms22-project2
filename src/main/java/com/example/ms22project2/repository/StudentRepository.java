package com.example.ms22project2.repository;
import com.example.ms22project2.model.StudentModel;
import org.springframework.data.jpa.repository.JpaRepository;
public interface StudentRepository extends JpaRepository<StudentModel,Long>{
}