package com.example.ms22project2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class Ms22Project2Application {
	public static void main(String[] args) {
		SpringApplication.run(Ms22Project2Application.class, args);
	}
}