package com.example.ms22project2.service;
import com.example.ms22project2.dto.StudentDTO;
import com.example.ms22project2.model.StudentModel;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Set;
public interface StudentInterface {
    public StudentModel add(StudentDTO studentDTO);
    Set<StudentModel> getAll();
    StudentModel update(Long id,StudentDTO studentDTO);
    void delete(Long id);
}