package com.example.ms22project2.service;
import com.example.ms22project2.dto.StudentDTO;
import com.example.ms22project2.model.StudentModel;
import com.example.ms22project2.repository.StudentRepository;
import org.springframework.stereotype.Service;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
@Service
public class StudentImpl implements StudentInterface{
    private final StudentRepository studentRepository;
    public StudentImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }
    @Override
    public StudentModel add(StudentDTO studentDTO) {
        return studentRepository.save(new StudentModel(
                studentDTO.getName(),studentDTO.getSurname(),studentDTO.getAge()));
    }
    @Override
    public Set<StudentModel> getAll() {
        List<StudentModel> hr=studentRepository.findAll();
        return new HashSet<>(hr);
    }
    @Override
    public StudentModel update(Long id,StudentDTO studentDTO) {
        StudentModel student = studentRepository.findById(id).orElseThrow();
        if (studentDTO.getName() != null) {
            student.setName(studentDTO.getName());
        }
        if (studentDTO.getSurname() != null) {
            student.setSurname(studentDTO.getSurname());
        }
        if (studentDTO.getAge() != null) {
            student.setAge(studentDTO.getAge());
        }
        return studentRepository.save(student);
    }
    @Override
    public void delete(Long id) {
        StudentModel student = studentRepository.findById(id).orElseThrow();
        studentRepository.delete(student);
    }
}